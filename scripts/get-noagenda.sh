#!/usr/bin/env bash
# Usage:
#   ./get-noagenda.sh
# to list available episodes (then you can play them  
# url's with mpv player), or
#   ./get-noagenda.sh listen
# to listen to the last episode.

rss=https://mp3s.nashownotes.com/rss.xml
#rss=https://feed.podbean.com/bowlafterbowl/feed.xml

list=$(curl -s $rss \
  | sed -E 's/(<\/[^>]+>)/\1\n/g' \
  | grep -A1 -E '<item>|enclosure .*url=' \
  | grep -E '<title>|enclosure .*url=' \
  | sed -E 's/.*url\=\"([^\"]+).*/\1/' \
  | sed -E 's/.*>([^<]+).*/\1 - /' \
  | tr -d \\n \
  | sed -E 's/\.mp3/\.mp3\n/g')

if [[ "$1" == "listen" ]] || [[ "$1" == "last" ]] ; then
  first_mp3=$(echo $list \
    | sed -E 's/([^\ |>]\.mp3).*$/\1/' \
    | sed -E 's/^.*(http.*)/\1/'
  )
  mpv --no-audio-display $first_mp3
else
  echo ""
  echo "Latest episodes:"
  echo ""
  echo $list | sed -E 's/\.mp3 ?/\.mp3\n/g'
fi

exit